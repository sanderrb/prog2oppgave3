package com.example;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class testPlayingCard {

    @Test
    public void testGetSuit() {
        PlayingCard card = new PlayingCard('S', 5);
        assertEquals("Suit should be 'S'", 'S', card.getSuit());
    }

    @Test
    public void testGetValue() {
        PlayingCard card = new PlayingCard('S', 5);
        assertEquals("Value should be 5", 5, card.getValue());
    }

    @Test
    public void testToString() {
        PlayingCard card = new PlayingCard('S', 5);
        assertEquals("String representation should be 'S5'", "S5", card.toString());
    }
}
