package com.example;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.Test;
import java.util.List;

public class testDeckOfCards {
    private DeckOfCards deck;

    @Before
    public void setUp() {
        deck = new DeckOfCards();
    }

    @Test
    public void testDealHand() {
        List<PlayingCard> hand = deck.dealHand(5);
        assertEquals("Dealt hand should contain 5 cards", 5, hand.size());
        assertEquals("Deck should have 47 cards remaining after dealing 5 cards", 47, deck.cardsRemaining());
    }

    @Test(expected = IllegalStateException.class)
    public void testDealHandNotEnoughCards() {
        // Ensure dealing more cards than available throws IllegalStateException
        deck.dealHand(60);
    }

    @Test
    public void testResetDeck() {
        deck.dealHand(10); // Deal some cards
        deck.resetDeck();
        assertEquals("Reset deck should have 52 cards", 52, deck.cardsRemaining());
    }

    @Test
    public void testCardsRemaining() {
        assertEquals("Newly created deck should have 52 cards", 52, deck.cardsRemaining());
        deck.dealHand(10);
        assertEquals("After dealing 10 cards, deck should have 42 cards remaining", 42, deck.cardsRemaining());
        deck.resetDeck();
        assertEquals("After resetting the deck, it should have 52 cards again", 52, deck.cardsRemaining());
    }
}
