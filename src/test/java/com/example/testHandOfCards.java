package com.example;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.Test;
import java.util.Arrays;
import java.util.List;

public class testHandOfCards {
    private HandOfCards hand;

    @Before
    public void setUp() {
        List<PlayingCard> cards = Arrays.asList(
            new PlayingCard('S', 1),
            new PlayingCard('S', 2),
            new PlayingCard('S', 3),
            new PlayingCard('S', 4),
            new PlayingCard('S', 5)
        );
        hand = new HandOfCards(cards);
    }

    @Test
    public void testHasFlush() {
        assertTrue("Hand should have a flush", hand.hasFlush());
    }


    @Test
    public void testSumOfFaces() {
        assertEquals("Sum of face values should be 15", 15, hand.sumOfFaces());
    }


    @Test
    public void testHasCard() {
        assertTrue("Hand should have card '2S'", hand.hasCard('S', 2));
        assertFalse("Hand should not have card '6S'", hand.hasCard('S', 6));
    }
}
