package com.example;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class HandOfCards {
    private final List<PlayingCard> cards;

    public HandOfCards(List<PlayingCard> cards) {
        this.cards = cards;
    }

    public boolean hasFlush() {
        Map<Character, Long> suitCount = cards.stream()
                .collect(Collectors.groupingBy(PlayingCard::getSuit, Collectors.counting()));

        return suitCount.values().stream().anyMatch(count -> count >= 5);
    }

    @Override
    public String toString() {
        return cards.stream()
                .map(PlayingCard::toString)
                .collect(Collectors.joining(" "));
    }

    public int sumOfFaces() {
        return cards.stream().mapToInt(PlayingCard::getValue).sum();
    }

    public String cardsOfSuit(char suit) {
        return cards.stream()
                .filter(card -> card.getSuit() == suit)
                .map(PlayingCard::toString)
                .collect(Collectors.joining(" "));
    }

    public boolean hasCard(char suit, int value) {
        return cards.stream().anyMatch(card -> card.getSuit() == suit && card.getValue() == value);
    }
}

