package com.example;

import javax.swing.*;
import java.awt.*;


public class CardGameGUI extends JFrame {
    private final JTextArea handDisplay;
    private final JButton dealButton;
    private final JButton resetButton;
    private final JLabel sumLabel;
    private final JLabel heartsLabel;
    private final JLabel flushLabel;
    private final JLabel queenOfSpadesLabel;
    
    private DeckOfCards deck;
    private HandOfCards hand;

    public CardGameGUI() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Card Game");
        setSize(600, 400);
        setLayout(new BorderLayout());

        handDisplay = new JTextArea(10, 40);
        handDisplay.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(handDisplay);

        dealButton = new JButton("Deal hand");
        resetButton = new JButton("Reset Deck");
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(dealButton);
        buttonPanel.add(resetButton); 
        
        sumLabel = new JLabel("Sum of the faces: 0");
        heartsLabel = new JLabel("Cards of hearts: None");
        flushLabel = new JLabel("Flush: No");
        queenOfSpadesLabel = new JLabel("Queen of spades: No");
        
        JPanel statusPanel = new JPanel(new GridLayout(4, 1));
        statusPanel.add(sumLabel);
        statusPanel.add(heartsLabel);
        statusPanel.add(flushLabel);
        statusPanel.add(queenOfSpadesLabel);
        
        add(scrollPane, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.NORTH);
        add(statusPanel, BorderLayout.SOUTH);
        
        deck = new DeckOfCards();
        
        dealButton.addActionListener(e -> dealHand());
        resetButton.addActionListener(e -> resetDeckAndHand());
    }

    private void dealHand() {
        try {
            hand = new HandOfCards(deck.dealHand(5));
            handDisplay.setText(hand.toString());
            updateStatus();
        } catch (IllegalStateException e) {
            JOptionPane.showMessageDialog(this, "Not enough cards. Resetting deck.");
            resetDeckAndHand();
        }
    }

    private void resetDeckAndHand() {
        deck.resetDeck();
        handDisplay.setText("");
        hand = null;
        updateStatus();
    }

    private void updateStatus() {
        if (hand != null) {
            sumLabel.setText("Sum of the faces: " + hand.sumOfFaces());
            String hearts = hand.cardsOfSuit('H');
            heartsLabel.setText("Cards of hearts: " + (hearts.isEmpty() ? "None" : hearts));
            flushLabel.setText("Flush: " + (hand.hasFlush() ? "Yes" : "No"));
            queenOfSpadesLabel.setText("Queen of spades: " + (hand.hasCard('S', 12) ? "Yes" : "No"));
        } else {
            sumLabel.setText("Sum of the faces: 0");
            heartsLabel.setText("Cards of hearts: None");
            flushLabel.setText("Flush: No");
            queenOfSpadesLabel.setText("Queen of spades: No");
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            CardGameGUI frame = new CardGameGUI();
            frame.setVisible(true);
        });
    }
}
